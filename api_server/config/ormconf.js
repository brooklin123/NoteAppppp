import user from "./../entity/user.js";
export default {
    type: "mysql",
    host: "mysql",
    port: 3306,
    username: "admin",
    password: "password",
    database: "myDB",
    logging: true,
    synchronize: true,
    entities:[
        user,
    ]
}