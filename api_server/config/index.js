import dotenv from "dotenv";
import mysql from "mysql";

// Set the NODE_ENV to 'development' by default
process.env.NODE_ENV = process.env.NODE_ENV || "development";

const envFound = dotenv.config();
if (envFound.error) {
  throw new Error(".env file not found");
}


//db config  寫在.env
//js物件
export default {
  port: parseInt(process.env.PORT, 10), //解析第一個參數，而他是十進位
  api: {
    prefix: "/api",
  },
};

