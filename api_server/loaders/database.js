import { createConnection } from "typeorm";
import config from "./../config/ormconf.js";
export default async () => {
    console.log("try connection");
    await createConnection(config).catch((e) => {
        console.log(`Database: ${e}`);
        process.exit(-1);
    });
};