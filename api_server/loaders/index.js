import express from "express";
import api from "./../API/index.js";
import config from "./../config/index.js";
import databaseLoader from "./database.js";
export default async(app) => {
    console.log("here is loader!");
    await databaseLoader();
    // 建立 Roruter 物件
    var router = express.Router();
    //當作middleware~ 在處理請求前都會型執行此
    app.use('/', router);

    router.get('/', (req, res) => {
        res.send('hello world!')
    });
    router.get('/bye', (req, res) => {
        res.send("bye!")
    });
  
    app.use(config.api.prefix, api());

 };
