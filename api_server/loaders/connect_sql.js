import mysql from "mysql";
import config from "./";
export default()=>{
    const database = mysql.createConnection({
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME
    });
});
    //https://betterprogramming.pub/how-to-use-mysql-with-node-js-and-docker-7dfc10860e7c
}
