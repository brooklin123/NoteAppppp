import { Router, urlencoded } from "express";
import hello from "./routers/hello.js";
export default () => {
    const app = Router();
    app.use(urlencoded({ extended: true }));
    hello(app);
    return app;
}