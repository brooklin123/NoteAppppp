import {Router} from "express";

const route = Router();
export default (app) => {
  app.use("/hello", route);

  route.get("/hi", (req, res) => {
    res.status(200).json({message: 'HI~~~'});
  });
}