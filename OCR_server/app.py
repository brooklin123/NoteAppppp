from flask import Flask, request, make_response
from http import client
import os, io
from google.cloud import vision


app = Flask(__name__)

def detection(FILE_PATH):
    global client 
    
    # Loads the image into memory
    with io.open(FILE_PATH, 'rb') as image_file:
        content = image_file.read()

    image = vision.Image(content=content)
    response = client.document_text_detection(image=image)

    texts = response.full_text_annotation.text
    
    return texts




@app.route("/", methods=['GET'])
def index() :
    global client 
    
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = r'key.json'
    client = vision.ImageAnnotatorClient()


    # request sample: "http://192.168.1.114:8081/?image=ping.png"
    formData = request.args['image']
    file_path = "./picture/" + formData


    texts = detection(file_path)

    return make_response(texts, 200)


if __name__ == "__main__":
    # 啟動 server
    app.run(host='0.0.0.0', port=8081, threaded=True, debug=True)